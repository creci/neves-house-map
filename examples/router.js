import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router);


export const constantRoutes = [
    {
        path: '/Map/:id',
        name: 'text',
        component: () => import('./doc/Map/MapOnline'),
        meta: {title: '地图', icon: 'el-icon-location-information', index: '4'},
        children: [
            {
                path: '/MapOnline',
                name: '在线地图',
                component: () => import('./doc/Map/MapOnline'),
                meta: {title: '在线地图', icon: 'el-icon-map-location', index: '4-1'}
            },
            {
                path: '/MapTile',
                name: '瓦片地图',
                component: () => import('./doc/Map/MapTile'),
                meta: {title: '瓦片地图', icon: 'el-icon-location-outline', index: '4-2'}
            },
        ]
    },
]

export default new Router({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes: constantRoutes
})
