import Vue from 'vue'
import router from './router'
import axios from 'axios';
import 'element-ui/lib/theme-chalk/index.css'
import ElementUI from 'element-ui'


import App from './App.vue'

Vue.use(ElementUI);


/* 地图引入 */
import openlayermap, {createLayer, createVectorLayer, SLClusterLayer, removeOverLayeById} from 'vue-openlayer-map';

Vue.prototype.ClusterLayer = SLClusterLayer
Vue.use(openlayermap);

Vue.config.productionTip = false;
Vue.prototype.$ajax = axios;
new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
